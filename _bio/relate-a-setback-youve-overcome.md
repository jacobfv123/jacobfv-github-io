---
layout: page

topic: Relate a setback you've overcome
hidden: false
inline: false
order: 5
---

Getting up after my DIY'ed [Cookie Cutter CNC](https://jacobfvaldez.weebly.com/engineering.html#cookie-cutter) EDM head shorted out the lab in 9th grade in front of my entire class, teacher, and principal was probably the hardest, most embarassing setback I've faced. I'd spent dozens of hours, written [hundreds of lines of code](https://github.com/JacobFV/CookieControl), learned so many fundamental science and engineering concepts, and the reward was very discouraging. But I couldn't give up on engineering — or even that project. The very next day, [I presented](https://youtu.be/yGfpX0WXYFM) on Cookie Cutter project and its present and planned features. Throughout the Summer, I quenched my newly ignited passion for calculus, differential equations, fluid dynamics, electricity, and material science with hundreds of textbook pages, documents, and wikipedia articles. And with similar determination, it is still my resolve to carry motivational integrity through the setbacks I face as an engineer, and more generally, as a human.

Oh, and since you clicked to this page, here's another one: I've recently had to change my long term plans for my robot. You see, I've been working on this robot called the [limboid](https://limboid.ai) for the past 6 months with the idea that I'd have something functional to show investors or customers or whoever and look back on my pull-of-the-lever and feel good about how I spent 1/150th of my life. But I've had to accept that I currently lack the sufficient combination of expertiese, personality, and finances to continue this endevour. So now I'm heading back to the workforce to see what I can do there. I really think I have a lot to give; I just hope they don't look at this failure and ignore me for it. We'll see.
