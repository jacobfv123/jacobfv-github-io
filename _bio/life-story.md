---
layout: page

topic: My life story
hidden: false
inline: true
order: 1
---

*Drafted from other content on this site using GPT4. Manually post-processed*:

My name is Jacob Valdez, and my passion for artificial intelligence (AI) and technology has driven me throughout my life. I've always been fascinated by the potential of AI and its applications, and this passion has shaped my career and education.

I began my journey in computer science during primary school, learning VB6 in 5th grade and moving on to C# a year later. As I progressed through middle and high school, I continued to explore various programming languages and technologies, including HTML/CSS/JS, TypeScript, and Python. My curiosity and drive for learning never waned, and I became increasingly interested in the development of AI and its potential impact on the world.

I received my A.A.S. in Mathematics from Navarro College in 2018, and then went on to earn my B.S. in Computer Science from The University of Texas at Arlington in 2022. During my time at UTA, I worked as a student research assistant in various capacities, gaining valuable experience in fields such as machine learning, robotics, and web development.

My software engineering career began as a software engineer at Motio Inc, where I developed Soterre for Qlik Sense. Later, I left to spend some time developing humanoid robots. I had to put this project on the backburner to find work, but I continue to work on it in my spare time.

As an engineer, I have faced challenges and setbacks, but I have always remained committed to my passion for AI and technology. I'm really excited about the progress we're seeing in AI right now (Summer 2023). I hope to be at the forefront of these developments, pushing the boundaries of what is possible and working to create a brighter future for all.
