---
layout: page
title: more
nav: true
nav_order: 6
dropdown: true
children:
    - title: 📅 Schedule Meeting
      permalink: https://cal.com/jacob-valdez
    - title: 📃 resume
      permalink: /resume/
    - title: 🖥️ ml resume
      permalink: /ml/
    - title: 📝 papers
      permalink: /papers/
    - title: 📓 notebook ↗
      permalink: https://jacobvaldez.notion.site/86ffc91935534518845efe5ce99a939c?v=1e6186860ff746b5b057dc6d6164be7c
    - title: divider
    - title: 🧍‍♂️academic presentations ↗
      permalink: https://youtube.com/playlist?list=PLMkgx9jjZQweNa7NpIwTM5gl1UBKrs4rd
    - title: 🎥 older videos ↗
      permalink: https://jacobfvaldez.weebly.com/videos.html
    - title: 🛠️ older projects ↗
      permalink: https://jacobfvaldez.weebly.com/
---
