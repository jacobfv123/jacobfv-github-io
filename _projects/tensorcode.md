---
layout: page

title: "Tensor<code>Code</code>"

hidden:
redirect: https://github.com/Limboid/tensorcode
category: [ai]
importance: 1

date: 2022-10-19 # YYYY-MM-DD, must be specified
start: 2022-04-14
end:
display_date: # used instead of `date` or date range

img: /assets/img/tensorcode-logo.png
github: Limboid/tensorcode # uname/repo, don't include the prefix `https://github.com/`
under_construction: true

description: SE + ML = Programming 2.0
bullet_points: | # at least two bullet points
    - will provides simple abstractions and functions for encoding and decoding arbitrary python objects
    - will support differentiable programming (including differentiable control flow)
    - will introduce intelligent object creation / selection / other runtime code generation features
---
